package com.mle.lab2.model;

public interface Identifiable <I> {
    I getId();
    void setId(I id);
}
