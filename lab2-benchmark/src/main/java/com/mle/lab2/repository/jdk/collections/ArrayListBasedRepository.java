package com.mle.lab2.repository.jdk.collections;

import com.mle.lab2.repository.AbstractJdkCollectionBasedRepository;

import java.util.ArrayList;

public class ArrayListBasedRepository <T> extends AbstractJdkCollectionBasedRepository<T> {
    public ArrayListBasedRepository() {
        super(new ArrayList<>());
    }
}
