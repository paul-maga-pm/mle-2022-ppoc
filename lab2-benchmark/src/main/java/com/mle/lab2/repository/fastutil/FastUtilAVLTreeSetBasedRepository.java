package com.mle.lab2.repository.fastutil;

import com.mle.lab2.repository.AbstractJdkCollectionBasedRepository;
import it.unimi.dsi.fastutil.objects.ObjectAVLTreeSet;

public class FastUtilAVLTreeSetBasedRepository<T> extends AbstractJdkCollectionBasedRepository<T> {

    public FastUtilAVLTreeSetBasedRepository() {
        super(new ObjectAVLTreeSet<>());
    }
}
