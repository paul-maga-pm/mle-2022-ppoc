package com.mle.lab2.repository.jdk.collections;

import com.mle.lab2.repository.AbstractJdkCollectionBasedRepository;

import java.util.HashSet;

public class HashSetBasedRepository <T extends Comparable<T>> extends AbstractJdkCollectionBasedRepository<T> {

    public HashSetBasedRepository() {
        super(new HashSet<>());
    }
}
