package com.mle.lab2.repository.jdk.map;

import com.mle.lab2.model.Identifiable;
import com.mle.lab2.repository.AbstractJdkMapBasedRepository;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository <I, T extends Identifiable<I>> extends AbstractJdkMapBasedRepository<I, T> {

    public ConcurrentHashMapBasedRepository() {
        super(new ConcurrentHashMap<>());
    }
}
