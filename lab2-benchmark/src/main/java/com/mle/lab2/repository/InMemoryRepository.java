package com.mle.lab2.repository;

public interface InMemoryRepository <T> {

    void add(T model);
    boolean contains(T model);
    void remove(T model);
}
