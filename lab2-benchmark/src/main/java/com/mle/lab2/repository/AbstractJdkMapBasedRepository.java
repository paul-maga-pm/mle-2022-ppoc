package com.mle.lab2.repository;

import com.mle.lab2.model.Identifiable;

import java.util.Map;

public abstract class AbstractJdkMapBasedRepository <I, T extends Identifiable<I>> implements InMemoryRepository<T> {

    private final Map<I, T> elements;

    public AbstractJdkMapBasedRepository(Map<I, T> elements) {
        this.elements = elements;
    }

    @Override
    public void add(T model) {
        if (!this.elements.containsKey(model.getId())) {
            this.elements.put(model.getId(), model);
        }
    }

    @Override
    public boolean contains(T model) {
        return this.elements.containsKey(model.getId());
    }

    @Override
    public void remove(T model) {
        this.elements.remove(model.getId());
    }
}
