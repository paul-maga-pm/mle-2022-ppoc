package com.mle.lab2.repository.eclipse;

import com.mle.lab2.repository.AbstractJdkCollectionBasedRepository;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;



public class EclipseUnifiedSetBasedRepository<T> extends AbstractJdkCollectionBasedRepository<T> {

    public EclipseUnifiedSetBasedRepository() {
        super(new UnifiedSet<>());
    }
}
