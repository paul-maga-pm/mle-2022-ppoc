package com.mle.lab2.repository;

import java.util.Collection;

public abstract class AbstractJdkCollectionBasedRepository<T> implements InMemoryRepository<T> {

    private final Collection<T> elements;

    public AbstractJdkCollectionBasedRepository(Collection<T> elements) {
        this.elements = elements;
    }

    @Override
    public void add(T model) {
        if (this.elements.contains(model)) {
            return;
        }
        this.elements.add(model);
    }

    @Override
    public boolean contains(T model) {
        return this.elements.contains(model);
    }

    @Override
    public void remove(T model) {
        this.elements.remove(model);
    }
}
