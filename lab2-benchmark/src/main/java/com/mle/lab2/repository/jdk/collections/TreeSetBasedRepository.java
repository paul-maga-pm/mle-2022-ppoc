package com.mle.lab2.repository.jdk.collections;

import com.mle.lab2.repository.AbstractJdkCollectionBasedRepository;

import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Comparable<T>> extends AbstractJdkCollectionBasedRepository<T> {

    public TreeSetBasedRepository() {
        super(new TreeSet<>());
    }
}
