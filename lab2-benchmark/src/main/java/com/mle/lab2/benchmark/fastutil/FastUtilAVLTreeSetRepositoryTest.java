package com.mle.lab2.benchmark.fastutil;

import com.mle.lab2.benchmark.AbstractInMemoryRepositoryBenchmarkTest;
import com.mle.lab2.model.Order;
import com.mle.lab2.repository.InMemoryRepository;
import com.mle.lab2.repository.fastutil.FastUtilAVLTreeSetBasedRepository;

public class FastUtilAVLTreeSetRepositoryTest extends AbstractInMemoryRepositoryBenchmarkTest {
    @Override
    public InMemoryRepository<Order> createRepository() {
        return new FastUtilAVLTreeSetBasedRepository<>();
    }


}
