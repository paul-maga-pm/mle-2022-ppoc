package com.mle.lab2.benchmark.eclipse;

import com.mle.lab2.benchmark.AbstractInMemoryRepositoryBenchmarkTest;
import com.mle.lab2.model.Order;
import com.mle.lab2.repository.InMemoryRepository;
import com.mle.lab2.repository.eclipse.EclipseUnifiedSetBasedRepository;

public class EclipseUnifiedCollectionBenchmarkTest extends AbstractInMemoryRepositoryBenchmarkTest {
    @Override
    public InMemoryRepository<Order> createRepository() {
        return new EclipseUnifiedSetBasedRepository<>();
    }

}
