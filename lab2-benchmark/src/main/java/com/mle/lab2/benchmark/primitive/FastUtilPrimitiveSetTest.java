package com.mle.lab2.benchmark.primitive;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;

import java.util.Set;

public class FastUtilPrimitiveSetTest extends AbstractPrimitiveSetTest {
    @Override
    protected Set<Integer> createEmptySet() {
        return new IntOpenHashSet();
    }

}
