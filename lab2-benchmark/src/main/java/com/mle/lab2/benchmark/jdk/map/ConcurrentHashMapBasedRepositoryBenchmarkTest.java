package com.mle.lab2.benchmark.jdk.map;

import com.mle.lab2.benchmark.AbstractInMemoryRepositoryBenchmarkTest;
import com.mle.lab2.model.Order;
import com.mle.lab2.repository.InMemoryRepository;
import com.mle.lab2.repository.jdk.map.ConcurrentHashMapBasedRepository;

public class ConcurrentHashMapBasedRepositoryBenchmarkTest extends AbstractInMemoryRepositoryBenchmarkTest {

    @Override
    public InMemoryRepository<Order> createRepository() {
        return new ConcurrentHashMapBasedRepository<>();
    }
}
