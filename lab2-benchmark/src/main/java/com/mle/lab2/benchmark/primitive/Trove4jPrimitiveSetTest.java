package com.mle.lab2.benchmark.primitive;

import gnu.trove.decorator.TIntSetDecorator;
import gnu.trove.set.hash.TIntHashSet;

import java.util.Set;

public class Trove4jPrimitiveSetTest extends AbstractPrimitiveSetTest{
    @Override
    protected Set<Integer> createEmptySet() {
        return new TIntSetDecorator(new TIntHashSet());
    }

}
