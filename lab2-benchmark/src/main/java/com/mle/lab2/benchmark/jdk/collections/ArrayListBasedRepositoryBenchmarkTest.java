package com.mle.lab2.benchmark.jdk.collections;

import com.mle.lab2.benchmark.AbstractInMemoryRepositoryBenchmarkTest;
import com.mle.lab2.model.Order;
import com.mle.lab2.repository.InMemoryRepository;
import com.mle.lab2.repository.jdk.collections.ArrayListBasedRepository;

public class ArrayListBasedRepositoryBenchmarkTest extends AbstractInMemoryRepositoryBenchmarkTest {



    @Override
    public InMemoryRepository<Order> createRepository() {
        return new ArrayListBasedRepository<>();
    }
}
