package com.mle.lab2.benchmark;

import com.mle.lab2.model.Order;
import com.mle.lab2.repository.InMemoryRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Thread)
public abstract class AbstractInMemoryRepositoryBenchmarkTest {

    public InMemoryRepository<Order> orderRepository;

    @Param({"100", "1000", "10000", "100000"})
    public int repositorySize;
    public Order orderUsedForAddTest;
    public Order orderUsedForContainsTest;
    public Order orderUsedForRemoveTest;

    @Setup(Level.Invocation)
    public void setup() {
        orderRepository = createRepository();
        for (int i = 0; i < repositorySize; i++) {
            orderRepository.add(new Order(i, i, i));
        }
        orderUsedForAddTest = new Order(repositorySize, repositorySize, repositorySize);
        orderUsedForContainsTest = new Order(repositorySize / 2, repositorySize / 2, repositorySize / 2);
        orderUsedForRemoveTest = new Order(0, 0, 0);
    }

    @Benchmark
    public void testAdd() {
        orderRepository.add(orderUsedForAddTest);
    }

    @Benchmark
    public void testContains(Blackhole consumer) {
        consumer.consume(orderRepository.contains(orderUsedForContainsTest));
    }

    @Benchmark
    public void testRemove() {
        orderRepository.remove(orderUsedForRemoveTest);
    }

    public abstract InMemoryRepository<Order> createRepository();

}
