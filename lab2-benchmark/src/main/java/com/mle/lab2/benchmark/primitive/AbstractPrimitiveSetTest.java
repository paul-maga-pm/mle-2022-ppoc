package com.mle.lab2.benchmark.primitive;

import org.openjdk.jmh.annotations.*;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Thread)
public abstract class AbstractPrimitiveSetTest {

    @Param({"100", "1000", "10000", "100000", "1000000"})
    public int listSize;
    public Set<Integer> set;

    @Setup(Level.Invocation)
    public void setup() {
        set = createEmptySet();
        for (int i = 0; i < listSize; i++) {
            set.add(i);
        }
    }

    @Benchmark
    public void testAdd() {
        set.add(10);
    }

    @Benchmark
    public boolean testContains() {
        return set.contains(0);
    }

    @Benchmark
    public boolean testRemove() {
        return set.remove(listSize - 1);
    }

    protected abstract Set<Integer> createEmptySet();

}
