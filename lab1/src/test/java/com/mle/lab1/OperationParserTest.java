package com.mle.lab1;

import com.mle.lab1.exceptions.InvalidExpression;
import com.mle.lab1.exceptions.InvalidOperation;
import com.mle.lab1.model.*;
import junit.framework.TestCase;
import org.junit.Test;


import static org.junit.Assert.assertThrows;

public class OperationParserTest extends TestCase {

    OperationParser parser = OperationParser.getINSTANCE();

    public void testParseAdditionExpression() {

        String[] expression = new String[] {"5", "+", "6"};
        Addition addition = (Addition) parser.parseStringToOperation(expression);
        assertEquals(11.0, addition.calculate());
    }

    public void testParseSubtractionExpression() {

        String[] expression = new String[] {"5", "-", "6"};
        Subtraction addition = (Subtraction) parser.parseStringToOperation(expression);
        assertEquals(-1.0, addition.calculate());
    }

    public void testParseMultiplicationExpression() {

        String[] expression = new String[] {"5", "*", "6"};
        Multiplication operation = (Multiplication) parser.parseStringToOperation(expression);
        assertEquals(30.0, operation.calculate());
    }

    public void testParseDivisionExpression() {

        String[] expression = new String[] {"10", "/", "2"};
        Division operation = (Division) parser.parseStringToOperation(expression);
        assertEquals(5.0, operation.calculate());
    }

    public void testThrowsArithmeticException() {
        String[] expression = new String[] {"10", "/", "0"};

        assertThrows(ArithmeticException.class, () -> {
            var op = parser.parseStringToOperation(expression);
            op.calculate();
        });
    }

    @Test
    public void testThrowsInvalidExpression() {
        var expr1 = new String[] {"10", "10"};
        var expr2 = new String[] {"adsd", "ads", "ads"};
        var expr3 = new String[] {"10.0", "asd", "10.0"};

        assertThrows(InvalidExpression.class, ()->{
            parser.parseStringToOperation(expr1);
        });

        assertThrows(InvalidExpression.class, ()->{
            parser.parseStringToOperation(expr2);
        });

        assertThrows(InvalidOperation.class, ()->{
            parser.parseStringToOperation(expr3);
        });

    }

}