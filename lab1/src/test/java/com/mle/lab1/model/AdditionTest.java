package com.mle.lab1.model;

import junit.framework.TestCase;

public class AdditionTest extends TestCase {

    public void testCalculate() {
        Addition addition = new Addition(5, 5);
        assertEquals(10.0, addition.calculate());
    }
}