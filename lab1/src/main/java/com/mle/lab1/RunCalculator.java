package com.mle.lab1;

import com.mle.lab1.exceptions.BaseException;

public class RunCalculator {

    public static void main(String[] args) {
        var parser = OperationParser.getINSTANCE();

        try {
            System.out.println(parser.parseStringToOperation(args).calculate());
        } catch (ArithmeticException | BaseException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
