package com.mle.lab1.model;

public class Addition implements Operation{

    private double x;
    private double y;

    public Addition(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double calculate() {
        return x + y;
    }
}
