package com.mle.lab1.model;

public class Multiplication implements Operation {

    private double x;
    private double y;

    public Multiplication(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double calculate() throws ArithmeticException {
        return x * y;
    }
}
