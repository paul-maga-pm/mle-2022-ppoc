package com.mle.lab1.model;


public class Division implements Operation {

    double x;
    double y;

    public Division(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double calculate() throws ArithmeticException {

        if (y == 0)
            throw new ArithmeticException("Division by 0 not allowed!");
        return x / y;
    }
}
