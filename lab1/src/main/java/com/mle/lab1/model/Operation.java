package com.mle.lab1.model;

public interface Operation {

    double calculate() throws ArithmeticException;
}
