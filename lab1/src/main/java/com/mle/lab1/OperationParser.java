package com.mle.lab1;

import com.mle.lab1.exceptions.InvalidExpression;
import com.mle.lab1.exceptions.InvalidOperation;
import com.mle.lab1.factory.OperationFactory;
import com.mle.lab1.factory.OperationType;
import com.mle.lab1.model.Operation;

import java.util.HashMap;
import java.util.Map;

public class OperationParser {

    private static OperationParser INSTANCE = null;
    private static final OperationFactory operationFactory = OperationFactory.getINSTANCE();

    private static final Map<String, OperationType> OPERATION_TYPE_MAP = new HashMap<>();
    static {
        OPERATION_TYPE_MAP.put("+", OperationType.ADDITION);
        OPERATION_TYPE_MAP.put("-", OperationType.SUBTRACTION);
        OPERATION_TYPE_MAP.put("*", OperationType.MULTIPLICATION);
        OPERATION_TYPE_MAP.put("/", OperationType.DIVISION);
    }

    private OperationParser() {

    }

    public static OperationParser getINSTANCE() {
        if (INSTANCE == null)
            INSTANCE = new OperationParser();
        return INSTANCE;
    }

    public Operation parseStringToOperation(String[] expression) {

        if (expression.length != 3)
            throw new InvalidExpression("Invalid number of arguments");

        double x = 0;
        double y = 0;
        try {
            x = Double.parseDouble(expression[0]);
            y = Double.parseDouble(expression[2]);
        } catch (NumberFormatException numberFormatException) {
            throw new InvalidExpression("Invalid double format!");
        }

        OperationType type = OPERATION_TYPE_MAP.get(expression[1]);

        if (type == null)
            throw new InvalidOperation("Invalid operator!");

        return operationFactory.getOperation(type, x, y);
    }
}
