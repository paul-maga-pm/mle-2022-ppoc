package com.mle.lab1.exceptions;

public class InvalidExpression extends BaseException {

    public InvalidExpression() {
    }

    public InvalidExpression(String message) {
        super(message);
    }

    public InvalidExpression(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidExpression(Throwable cause) {
        super(cause);
    }

    public InvalidExpression(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
