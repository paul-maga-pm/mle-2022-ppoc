package com.mle.lab1.exceptions;

public class InvalidOperation extends BaseException {

    public InvalidOperation() {
    }

    public InvalidOperation(String message) {
        super(message);
    }

    public InvalidOperation(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidOperation(Throwable cause) {
        super(cause);
    }

    public InvalidOperation(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
