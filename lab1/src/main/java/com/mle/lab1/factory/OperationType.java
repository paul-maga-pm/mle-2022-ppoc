package com.mle.lab1.factory;

public enum OperationType {

    ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION
}
