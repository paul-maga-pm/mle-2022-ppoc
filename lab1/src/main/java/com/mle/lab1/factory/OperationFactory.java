package com.mle.lab1.factory;

import com.mle.lab1.model.*;

public class OperationFactory {

    private static OperationFactory INSTANCE = null;

    private OperationFactory() {

    }

    public static OperationFactory getINSTANCE() {

        if (INSTANCE == null)
            INSTANCE = new OperationFactory();

        return INSTANCE;
    }

    public Operation getOperation(OperationType type, double x, double y) {
        switch (type) {
            case ADDITION:
                return new Addition(x, y);

            case SUBTRACTION:
                return new Subtraction(x, y);

            case MULTIPLICATION:
                return new Multiplication(x, y);

            case DIVISION:
                return new Division(x, y);
            default:
                return null;
        }
    }
}
