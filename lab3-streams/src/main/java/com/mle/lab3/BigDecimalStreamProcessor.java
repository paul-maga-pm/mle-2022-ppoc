package com.mle.lab3;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.stream.Stream;

public class BigDecimalStreamProcessor {

    public BigDecimal getSum(Stream<BigDecimal> inputStream) {
        return inputStream.reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getAverage(Stream<BigDecimal> inputStream, BigDecimal noOfElements) {
        return getSum(inputStream).divide(noOfElements);
    }

    public Stream<BigDecimal> getFirst10PercentBigDecimals(Stream<BigDecimal> inputStream, long noOfElements) {
        return inputStream.sorted(Comparator.reverseOrder())
                .limit(noOfElements / 10);
    }
}
