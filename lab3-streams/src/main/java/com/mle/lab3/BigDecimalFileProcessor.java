package com.mle.lab3;

import java.io.*;
import java.math.BigDecimal;
import java.util.stream.Stream;

public class BigDecimalFileProcessor {

    private BigDecimalStreamProcessor streamProcessor = new BigDecimalStreamProcessor();
    private long noOfElementsRead = 0;

    private Stream<BigDecimal> readStreamFromFile(String inputFileName) {
        Stream.Builder<BigDecimal> builder = Stream.builder();

        try (ObjectInputStream fin = new ObjectInputStream(new FileInputStream(inputFileName))) {

            long noOfElements = fin.readLong();
            this.noOfElementsRead = noOfElements;
            for (int i = 0; i < noOfElements; i++) {
                BigDecimal currentElem = (BigDecimal) fin.readObject();
                builder.add(currentElem);
            }

            return builder.build();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void writeSumToFile(String inputFileName, String outputFileName) {

        try (ObjectOutputStream fout = new ObjectOutputStream(new FileOutputStream(outputFileName))) {
            Stream<BigDecimal> inputStream = readStreamFromFile(inputFileName);
            BigDecimal sum = streamProcessor.getSum(inputStream);
            fout.writeObject(sum);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeAverageToFile(String inputFileName, String outputFileName) {
        try (ObjectOutputStream fout = new ObjectOutputStream(new FileOutputStream(outputFileName))) {
            Stream<BigDecimal> inputStream = readStreamFromFile(inputFileName);
            BigDecimal average = streamProcessor.getAverage(inputStream, BigDecimal.valueOf(this.noOfElementsRead));
            fout.writeObject(average);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFirst10PercentBigDecimals(String inputFileName, String outputFileName) {
        try (ObjectOutputStream fout = new ObjectOutputStream(new FileOutputStream(outputFileName))) {
            Stream<BigDecimal> inputStream = readStreamFromFile(inputFileName);

            var resultStream = streamProcessor.getFirst10PercentBigDecimals(inputStream, noOfElementsRead);

            fout.writeLong(noOfElementsRead / 10);
            resultStream.forEach(elem -> {
                try {
                    fout.writeObject(elem);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
