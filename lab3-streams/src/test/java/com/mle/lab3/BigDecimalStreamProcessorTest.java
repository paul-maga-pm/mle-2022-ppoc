package com.mle.lab3;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BigDecimalStreamProcessorTest {

    private final BigDecimalStreamProcessor processor = new BigDecimalStreamProcessor();


    @Test
    void givenNonEmptyStream_whenGetSumIsCalled_thenNonZeroSumIsReturned() {
        Stream<BigDecimal> inputStream = Stream.of(
                BigDecimal.valueOf(1),
                BigDecimal.valueOf(2),
                BigDecimal.valueOf(3),
                BigDecimal.valueOf(4),
                BigDecimal.valueOf(5)
        );

        assertEquals(BigDecimal.valueOf(15), processor.getSum(inputStream));
    }

    @Test
    void givenEmptyStream_whenGetSumIsCalled_thenZeroIsReturned() {
        assertEquals(BigDecimal.ZERO, processor.getSum(Stream.<BigDecimal>builder().build()));
    }

    @Test
    void givenNonEmptyStream_whenGetAverageIsCalled_thenAverageIsReturned() {
        Stream<BigDecimal> inputStream = Stream.of(
                BigDecimal.valueOf(1),
                BigDecimal.valueOf(2),
                BigDecimal.valueOf(3),
                BigDecimal.valueOf(4),
                BigDecimal.valueOf(5)
        );
        assertEquals(BigDecimal.valueOf(3), processor.getAverage(inputStream, BigDecimal.valueOf(5)));

    }

    @Test
    void givenNonEmptyStream_getFirst10PercentBigDecimals_thenNonEmptyStreamIsReturned() {

        List<BigDecimal> inputList = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            inputList.add(BigDecimal.valueOf(i));
        }

        Collections.shuffle(inputList);

        Stream<BigDecimal> actualOutputStream = processor.getFirst10PercentBigDecimals(inputList.stream(), 1000);

        List<BigDecimal> actualOutputList = actualOutputStream.collect(Collectors.toList());

        assertEquals(100, actualOutputList.size());
        for (int i = 999; i >= 900; i--) {
            assertEquals(BigDecimal.valueOf(i), actualOutputList.get(999 - i));
        }
    }
}