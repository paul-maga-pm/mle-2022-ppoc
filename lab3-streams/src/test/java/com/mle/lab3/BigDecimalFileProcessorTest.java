package com.mle.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BigDecimalFileProcessorTest {

    private final BigDecimalFileProcessor fileProcessor = new BigDecimalFileProcessor();
    private static final String TEST_IN = "test.in";
    private static final String TEST_OUT = "test.out";

    private static final long SIZE = 1000;
    @AfterEach
    public void tearDown() throws IOException {
        Path path = Path.of(TEST_OUT);
        if (Files.exists(path)) {
            Files.delete(path);
        }
    }

    @BeforeAll
    public static void setUp() {
        final List<BigDecimal> inputList = new ArrayList<>();

        for (long i = 1; i <= SIZE; i++) {
            inputList.add(BigDecimal.valueOf(i));
        }

        Path path = Path.of(TEST_IN);
        if (Files.exists(path)) {
            try {
                Files.delete(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Collections.shuffle(inputList);
        try (ObjectOutputStream fout = new ObjectOutputStream(new FileOutputStream(TEST_IN))) {
            fout.writeLong(inputList.size());
            for (var elem : inputList) {
                fout.writeObject(elem);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetSum() {

        fileProcessor.writeSumToFile(TEST_IN, TEST_OUT);

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(TEST_OUT))) {

            System.out.println("*********Get sum**********************************");
            BigDecimal sum = (BigDecimal) ois.readObject();

            System.out.println("Sum = " + sum);

            BigDecimal expectedSum = BigDecimal.valueOf(SIZE)
                    .multiply(BigDecimal.valueOf(SIZE + 1))
                    .divide(BigDecimal.valueOf(2));

            assertEquals(expectedSum, sum);

            System.out.println("*********Get sum passed***************************");

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetAverage() {
        fileProcessor.writeAverageToFile(TEST_IN, TEST_OUT);

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(TEST_OUT))) {

            System.out.println("*********Get average******************************");
            BigDecimal average = (BigDecimal) ois.readObject();

            System.out.println("Average = " + average);

            BigDecimal expectedAverage = BigDecimal.valueOf(SIZE)
                    .multiply(BigDecimal.valueOf(SIZE + 1))
                    .divide(BigDecimal.valueOf(2))
                    .divide(BigDecimal.valueOf(SIZE));

            assertEquals(expectedAverage, average);

            System.out.println("*********Get average passed***********************");

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetFirst10Percent() {
        fileProcessor.writeFirst10PercentBigDecimals(TEST_IN, TEST_OUT);

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(TEST_OUT))) {

            System.out.println("*********Biggest 10 percent big decimals**************");
            long noOfElements = ois.readLong();

            for (long i = 0; i < noOfElements; i++) {
                var elem = (BigDecimal) ois.readObject();
                assertEquals(BigDecimal.valueOf(SIZE).subtract(BigDecimal.valueOf(i)), elem);
            }

            System.out.println("*********Biggest 10 percent big decimals passed*******");

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}