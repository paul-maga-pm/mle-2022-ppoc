package com.tora.chatapp.ui;

import com.tora.chatapp.core.service.api.EnterChatRoomService;
import com.tora.chatapp.core.service.api.LeaveChatRoomService;
import com.tora.chatapp.core.service.api.SendMessageInChatRoomService;
import com.tora.chatapp.core.service.events.MessageWasSendEvent;
import com.tora.chatapp.core.service.events.Observer;
import com.tora.chatapp.core.service.requests.EnterChatRoomRequest;
import com.tora.chatapp.core.service.requests.LeaveChatRoomRequest;
import com.tora.chatapp.core.service.requests.SendMessageInChatRoomRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleUi implements Observer<MessageWasSendEvent> {

    private final EnterChatRoomService enterChatRoomService;
    private final LeaveChatRoomService leaveChatRoomService;
    private final SendMessageInChatRoomService sendMessageInChatRoomService;

    public ConsoleUi(EnterChatRoomService enterChatRoomService,
                     LeaveChatRoomService leaveChatRoomService,
                     SendMessageInChatRoomService sendMessageInChatRoomService) {
        this.enterChatRoomService = enterChatRoomService;
        this.leaveChatRoomService = leaveChatRoomService;
        this.sendMessageInChatRoomService = sendMessageInChatRoomService;
    }

    public void run() {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println("login");
                System.out.println("exit");

                String cmd = reader.readLine();

                if (cmd.compareTo("exit") == 0) {
                    break;
                }

                if (cmd.compareTo("login") != 0) {
                    System.out.println("invalid command");
                    continue;
                }

                System.out.println("enter username");
                String username = reader.readLine();

                enterChatRoomService.enterChatRoom(this, new EnterChatRoomRequest(username));
                System.out.println("enter <exit> to leave chat");

                while (true) {

                    String input = reader.readLine();

                    if (input.compareTo("exit") == 0) {
                        break;
                    }

                    sendMessageInChatRoomService.sendMessage(new SendMessageInChatRoomRequest(input, username));
                }

                leaveChatRoomService.leaveChatRoom(this, new LeaveChatRoomRequest(username));

            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void update(MessageWasSendEvent event) {
        System.out.println(event.getSentMessage().getText());
    }
}
