package com.tora.chatapp.core.service.requests;

import java.io.Serializable;

public class EnterChatRoomRequest implements Serializable, Request {

    private String username;

    public EnterChatRoomRequest(String username) {
        this.username = username;
    }

    public EnterChatRoomRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
