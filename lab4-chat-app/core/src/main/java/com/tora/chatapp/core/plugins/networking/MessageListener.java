package com.tora.chatapp.core.plugins.networking;

import com.tora.chatapp.core.service.events.ConnectionWasClosedEvent;
import com.tora.chatapp.core.service.events.MessageWasSendEvent;
import com.tora.chatapp.core.service.events.Observable;
import com.tora.chatapp.core.service.events.Observer;

public abstract class MessageListener  {

    protected final Observable<MessageWasSendEvent> messageWasSendEventObservable;
    protected final Observable<ConnectionWasClosedEvent> connectionWasClosedEventObservable;

    protected MessageListener() {
        messageWasSendEventObservable = new Observable<>();
        connectionWasClosedEventObservable = new Observable<>();
    }

    public abstract void start(Connection connection);

    public void addMessageObserver(Observer<MessageWasSendEvent> observer) {
        messageWasSendEventObservable.addObserver(observer);
    }

    public void addConnectionWasClosedObserver(Observer<ConnectionWasClosedEvent> observer) {
        connectionWasClosedEventObservable.addObserver(observer);
    }

    public void removeMessageObserver(Observer<MessageWasSendEvent> messageObserver) {
        messageWasSendEventObservable.removeObserver(messageObserver);
    }
}
