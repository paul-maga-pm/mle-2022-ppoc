package com.tora.chatapp.core.service.events;

public interface Observer<E extends Event> {

    void update(E event);
}
