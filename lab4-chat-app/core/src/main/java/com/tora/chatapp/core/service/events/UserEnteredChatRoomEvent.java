package com.tora.chatapp.core.service.events;

import com.tora.chatapp.core.plugins.networking.Connection;

public class UserEnteredChatRoomEvent implements Event {

    private Connection newConnection;

    public UserEnteredChatRoomEvent() {
    }

    public UserEnteredChatRoomEvent(Connection newConnection) {
        this.newConnection = newConnection;
    }

    public Connection getNewConnection() {
        return newConnection;
    }

    public void setNewConnection(Connection newConnection) {
        this.newConnection = newConnection;
    }
}
