package com.tora.chatapp.core.plugins.networking;

import com.tora.chatapp.core.service.events.Observable;
import com.tora.chatapp.core.service.events.Observer;
import com.tora.chatapp.core.service.events.UserEnteredChatRoomEvent;

public class AddConnectionObserver extends Observable<UserEnteredChatRoomEvent> implements Observer<UserEnteredChatRoomEvent> {

    private final ConnectionManager connectionManager;

    public AddConnectionObserver(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public void update(UserEnteredChatRoomEvent event) {
        connectionManager.add(event.getNewConnection());
    }
}
