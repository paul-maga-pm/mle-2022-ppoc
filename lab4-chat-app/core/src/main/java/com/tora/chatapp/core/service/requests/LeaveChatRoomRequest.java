package com.tora.chatapp.core.service.requests;

import java.io.Serializable;

public class LeaveChatRoomRequest implements Serializable, Request {

    private String username;

    public LeaveChatRoomRequest(String username) {
        this.username = username;
    }

    public LeaveChatRoomRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
