package com.tora.chatapp.core.service.api;

import com.tora.chatapp.core.service.events.MessageWasSendEvent;
import com.tora.chatapp.core.service.events.Observer;
import com.tora.chatapp.core.service.requests.EnterChatRoomRequest;

public interface EnterChatRoomService {

    void enterChatRoom(Observer<MessageWasSendEvent> messageObserver, EnterChatRoomRequest request);
}
