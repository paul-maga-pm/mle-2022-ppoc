package com.tora.chatapp.core.service.api;

import com.tora.chatapp.core.service.events.MessageWasSendEvent;
import com.tora.chatapp.core.service.events.Observer;
import com.tora.chatapp.core.service.requests.LeaveChatRoomRequest;

public interface LeaveChatRoomService {

    void leaveChatRoom(Observer<MessageWasSendEvent> messageListener, LeaveChatRoomRequest leaveRequest);
}
