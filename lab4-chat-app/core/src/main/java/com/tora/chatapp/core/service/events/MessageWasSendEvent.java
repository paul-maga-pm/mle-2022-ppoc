package com.tora.chatapp.core.service.events;

import com.tora.chatapp.core.service.requests.Message;

public class MessageWasSendEvent implements Event {

    private Message sentMessage;

    public MessageWasSendEvent() {
    }

    public MessageWasSendEvent(Message sentMessage) {
        this.sentMessage = sentMessage;
    }

    public Message getSentMessage() {
        return sentMessage;
    }

    public void setSentMessage(Message sentMessage) {
        this.sentMessage = sentMessage;
    }
}
