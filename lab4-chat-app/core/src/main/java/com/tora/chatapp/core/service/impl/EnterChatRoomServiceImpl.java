package com.tora.chatapp.core.service.impl;

import com.tora.chatapp.core.plugins.networking.ConnectionListener;
import com.tora.chatapp.core.plugins.networking.ConnectionManager;
import com.tora.chatapp.core.service.api.EnterChatRoomService;
import com.tora.chatapp.core.service.events.MessageWasSendEvent;
import com.tora.chatapp.core.service.events.Observer;
import com.tora.chatapp.core.service.requests.EnterChatRoomRequest;
import com.tora.chatapp.core.service.requests.Message;
import com.tora.chatapp.core.service.requests.MessageType;

public class EnterChatRoomServiceImpl implements EnterChatRoomService {

    private final ConnectionManager connectionManager;
    private final ConnectionListener listener;

    public EnterChatRoomServiceImpl(ConnectionManager connectionManager,
                                    ConnectionListener listener) {
        this.connectionManager = connectionManager;
        this.listener = listener;
    }

    @Override
    public void enterChatRoom(Observer<MessageWasSendEvent> messageObserver, EnterChatRoomRequest request) {
        connectionManager.openConnections();
        connectionManager.subscribeForMessages(messageObserver);
        connectionManager.startMessageListeners();
        var connectMessage = String.format("%s entered the chat", request.getUsername());
        Message message = new Message(connectMessage, MessageType.SEND);
        connectionManager.broadcast(message);
        listener.start();
    }
}
