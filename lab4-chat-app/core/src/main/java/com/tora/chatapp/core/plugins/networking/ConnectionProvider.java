package com.tora.chatapp.core.plugins.networking;


import java.util.Set;

public interface ConnectionProvider {

    Set<Connection> create();
}
