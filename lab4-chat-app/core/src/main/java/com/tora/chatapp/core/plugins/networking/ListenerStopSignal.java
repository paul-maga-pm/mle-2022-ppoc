package com.tora.chatapp.core.plugins.networking;

public class ListenerStopSignal {

    private boolean isDone = false;

    public boolean isDone() {
        return isDone;
    }

    public void done() {
        isDone = true;
    }
}
