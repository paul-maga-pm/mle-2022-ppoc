package com.tora.chatapp.core.plugins.networking;

import com.tora.chatapp.core.service.events.ConnectionWasClosedEvent;
import com.tora.chatapp.core.service.events.Observer;

public class RemoveConnectionObserver implements Observer<ConnectionWasClosedEvent> {

    private final ConnectionManager connectionManager;

    public RemoveConnectionObserver(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public void update(ConnectionWasClosedEvent event) {
        connectionManager.remove(event.getClosedConnection());
    }
}
