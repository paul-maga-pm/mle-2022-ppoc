package com.tora.chatapp.core.service.requests;

import java.io.Serializable;

public class SendMessageInChatRoomRequest implements Serializable, Request {

    private String text;
    private String senderUserName;

    public SendMessageInChatRoomRequest() {
    }

    public SendMessageInChatRoomRequest(String text, String senderUserName) {
        this.text = text;
        this.senderUserName = senderUserName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSenderUserName() {
        return senderUserName;
    }

    public void setSenderUserName(String senderUserName) {
        this.senderUserName = senderUserName;
    }
}
