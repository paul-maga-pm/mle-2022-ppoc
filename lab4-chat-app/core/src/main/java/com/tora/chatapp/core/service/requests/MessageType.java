package com.tora.chatapp.core.service.requests;

public enum MessageType {
    LOGIN,
    LOGOUT,
    SEND
}
