package com.tora.chatapp.core.plugins.networking;

import com.tora.chatapp.core.service.requests.Message;

public abstract class Connection {
    public abstract void close();
    public abstract void send(Message message);

    public abstract boolean isClosed();
}
