package com.tora.chatapp.core.service.requests;

import java.io.Serializable;

public class Message implements Serializable {
    private String text;
    private String sender;
    private MessageType type;

    public Message(String text, String sender, MessageType type) {
        this.text = text;
        this.sender = sender;
        this.type = type;
    }

    public Message(String text, MessageType type) {
        this.text = text;
        this.type = type;
    }

    public Message(MessageType type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
