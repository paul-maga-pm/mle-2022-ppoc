package com.tora.chatapp.core.service.api;

import com.tora.chatapp.core.service.requests.SendMessageInChatRoomRequest;

public interface SendMessageInChatRoomService {

    void sendMessage(SendMessageInChatRoomRequest request);
}
