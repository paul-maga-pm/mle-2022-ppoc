package com.tora.chatapp.core.plugins.networking;

import com.tora.chatapp.core.service.events.MessageWasSendEvent;
import com.tora.chatapp.core.service.events.Observer;
import com.tora.chatapp.core.service.requests.Message;

import java.util.HashSet;
import java.util.Set;

public class ConnectionManager {

    private final ConnectionProvider provider;
    private Set<Connection> connections;
    private final MessageListener listener;

    public ConnectionManager(ConnectionProvider provider, MessageListener listener) {
        super();
        this.provider = provider;
        this.listener = listener;
    }

    public synchronized void openConnections() {
        connections = provider.create();
    }

    public synchronized void startMessageListeners() {


        for (var connection : connections) {
            listener.start(connection);
        }
    }

    public synchronized void closeConnections() {

        for (var connection : connections) {
            connection.close();
        }
        connections = new HashSet<>();
    }
    public synchronized void broadcast(Message message) {
        for (var connection : connections) {
            connection.send(message);
        }
    }

    public synchronized void add(Connection newConnection) {
        connections.add(newConnection);
        listener.start(newConnection);
    }

    public synchronized void remove(Connection closedConnection) {
        connections.remove(closedConnection);
    }

    public synchronized void subscribeForMessages(Observer<MessageWasSendEvent> messageObserver) {
        listener.addMessageObserver(messageObserver);
    }

    public synchronized void unsubscribeForMessages(Observer<MessageWasSendEvent> messageObserver) {
        listener.removeMessageObserver(messageObserver);
    }

}
