package com.tora.chatapp.core.service.events;

import com.tora.chatapp.core.plugins.networking.Connection;

public class ConnectionWasClosedEvent implements Event{

    private Connection closedConnection;

    public ConnectionWasClosedEvent(Connection closedConnection) {
        this.closedConnection = closedConnection;
    }

    public ConnectionWasClosedEvent() {
    }

    public Connection getClosedConnection() {
        return closedConnection;
    }

    public void setClosedConnection(Connection closedConnection) {
        this.closedConnection = closedConnection;
    }
}
