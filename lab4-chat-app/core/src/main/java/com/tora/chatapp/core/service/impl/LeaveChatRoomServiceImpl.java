package com.tora.chatapp.core.service.impl;

import com.tora.chatapp.core.plugins.networking.ConnectionListener;
import com.tora.chatapp.core.plugins.networking.ConnectionManager;
import com.tora.chatapp.core.service.api.LeaveChatRoomService;
import com.tora.chatapp.core.service.events.MessageWasSendEvent;
import com.tora.chatapp.core.service.events.Observer;
import com.tora.chatapp.core.service.requests.LeaveChatRoomRequest;
import com.tora.chatapp.core.service.requests.Message;
import com.tora.chatapp.core.service.requests.MessageType;

public class LeaveChatRoomServiceImpl implements LeaveChatRoomService {

    private final ConnectionManager connectionManager;
    private final ConnectionListener connectionListener;

    public LeaveChatRoomServiceImpl(ConnectionManager connectionManager, ConnectionListener connectionListener) {
        this.connectionManager = connectionManager;
        this.connectionListener = connectionListener;
    }

    @Override
    public void leaveChatRoom(Observer<MessageWasSendEvent> messageListener, LeaveChatRoomRequest leaveRequest) {
        var text = String.format("%s left the chat", leaveRequest.getUsername());
        Message message = new Message(text, MessageType.SEND);

        connectionManager.broadcast(message);
        connectionListener.stop();
        connectionManager.broadcast(new Message(MessageType.LOGOUT));
        connectionManager.closeConnections();
        connectionManager.unsubscribeForMessages(messageListener);
    }
}
