package com.tora.chatapp.core.plugins.networking;

import com.tora.chatapp.core.service.events.Observable;
import com.tora.chatapp.core.service.events.UserEnteredChatRoomEvent;

public abstract class ConnectionListener extends Observable<UserEnteredChatRoomEvent> {

    public abstract void start();
    public abstract void stop();
}
