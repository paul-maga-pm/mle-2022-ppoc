package com.tora.chatapp.core.service.events;

import java.util.HashSet;
import java.util.Set;

public class Observable <E extends Event> {

    private final Set<Observer<E>> observers;

    public Observable() {
        this.observers = new HashSet<>();
    }

    public synchronized void addObserver(Observer<E> observer) {
        observers.add(observer);
    }

    public synchronized void removeObserver(Observer<E> observer) {
        observers.remove(observer);
    }

    public synchronized void notifyEventWasTriggered(E event) {
        for (var observer : observers) {
                observer.update(event);
        }
    }
}
