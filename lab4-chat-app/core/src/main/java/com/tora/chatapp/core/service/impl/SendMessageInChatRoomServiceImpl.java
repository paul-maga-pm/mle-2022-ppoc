package com.tora.chatapp.core.service.impl;

import com.tora.chatapp.core.plugins.networking.ConnectionManager;
import com.tora.chatapp.core.service.api.SendMessageInChatRoomService;
import com.tora.chatapp.core.service.requests.Message;
import com.tora.chatapp.core.service.requests.MessageType;
import com.tora.chatapp.core.service.requests.SendMessageInChatRoomRequest;

public class SendMessageInChatRoomServiceImpl implements SendMessageInChatRoomService {

    private final ConnectionManager connectionManager;

    public SendMessageInChatRoomServiceImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }


    @Override
    public void sendMessage(SendMessageInChatRoomRequest request) {
        var text = String.format("%s: %s", request.getSenderUserName(), request.getText());
        Message message = new Message(text, MessageType.SEND);
        connectionManager.broadcast(message);
    }
}
