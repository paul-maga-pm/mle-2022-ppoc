package com.tora.chatapp.application;


import java.io.IOException;

public class Run {


    public static void main(String[] args) throws IOException {
        Config config = new Config();
        var ui = config.consoleUi();
        ui.run();
    }

}
