package com.tora.chatapp.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tora.chatapp.core.plugins.networking.AddConnectionObserver;
import com.tora.chatapp.core.plugins.networking.ConnectionManager;
import com.tora.chatapp.core.plugins.networking.RemoveConnectionObserver;
import com.tora.chatapp.core.service.impl.EnterChatRoomServiceImpl;
import com.tora.chatapp.core.service.impl.LeaveChatRoomServiceImpl;
import com.tora.chatapp.core.service.impl.SendMessageInChatRoomServiceImpl;
import com.tora.chatapp.plugins.networking.serversocket.JdkServerSocketListener;
import com.tora.chatapp.plugins.networking.serversocket.JdkSocketConnectionProvider;
import com.tora.chatapp.plugins.networking.serversocket.JdkSocketMessageListener;
import com.tora.chatapp.ui.ConsoleUi;

import java.io.IOException;
import java.io.InputStream;

public class Config {

    private static ApplicationProperties applicationProperties;

    private ApplicationProperties applicationProperties() {
        if (applicationProperties == null) {
            ObjectMapper mapper = new ObjectMapper();
            InputStream is = ApplicationProperties.class.getResourceAsStream("/application-config.json");
            try {
                applicationProperties = mapper.readValue(is, ApplicationProperties.class);
            } catch (IOException e) {
                System.out.println("Can't load application properties");
                e.printStackTrace();
                System.exit(1);
            }
        }
        return applicationProperties;
    }


    public ConsoleUi consoleUi() throws IOException {
        var props = applicationProperties();
        var nodes = props.getNodeProperties();
        var connectionProvider = new JdkSocketConnectionProvider(nodes);
        var connectionListener = new JdkServerSocketListener(props.getServerPort());
        var messageListener = new JdkSocketMessageListener();
        var connectionManager = new ConnectionManager(connectionProvider, messageListener);

        var enterChatRoomService = new EnterChatRoomServiceImpl(connectionManager, connectionListener);
        var leaveChatRoomService = new LeaveChatRoomServiceImpl(connectionManager, connectionListener);
        var sendMessageService = new SendMessageInChatRoomServiceImpl(connectionManager);

        var addConnectionObserver = new AddConnectionObserver(connectionManager);
        var removeConnectionObserver = new RemoveConnectionObserver(connectionManager);

        connectionListener.addObserver(addConnectionObserver);
        messageListener.addConnectionWasClosedObserver(removeConnectionObserver);

        return new ConsoleUi(enterChatRoomService, leaveChatRoomService, sendMessageService);
    }
}
