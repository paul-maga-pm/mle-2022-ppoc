package com.tora.chatapp.application;

import com.tora.chatapp.plugins.networking.serversocket.JdkClientSocketConnectionProperties;

import java.util.Set;

public class ApplicationProperties {

    private int serverPort;
    private Set<JdkClientSocketConnectionProperties> nodeProperties;

    public ApplicationProperties() {
    }

    public ApplicationProperties(int serverPort,
                                 Set<JdkClientSocketConnectionProperties> nodeProperties) {
        this.serverPort = serverPort;
        this.nodeProperties = nodeProperties;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public Set<JdkClientSocketConnectionProperties> getNodeProperties() {
        return nodeProperties;
    }

    public void setNodeProperties(Set<JdkClientSocketConnectionProperties> nodeProperties) {
        this.nodeProperties = nodeProperties;
    }
}
