package com.tora.chatapp.plugins.networking.serversocket;

import com.tora.chatapp.core.plugins.networking.Connection;
import com.tora.chatapp.core.service.requests.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class JdkSocketClientConnection extends Connection {

    private final Socket internalSocket;
    private final ObjectInputStream inputReader;
    private final ObjectOutputStream outputWriter;
    public JdkSocketClientConnection(JdkClientSocketConnectionProperties properties) throws IOException {
        this.internalSocket = new Socket(properties.getIpAddress(), properties.getPort());
        this.outputWriter = new ObjectOutputStream(internalSocket.getOutputStream());
        this.inputReader = new ObjectInputStream(internalSocket.getInputStream());
    }

    public JdkSocketClientConnection(Socket socket) throws IOException {
        this.internalSocket = socket;
        this.inputReader = new ObjectInputStream(internalSocket.getInputStream());
        this.outputWriter = new ObjectOutputStream(internalSocket.getOutputStream());
    }

    public ObjectInputStream getInputReader() {
        return inputReader;
    }

    @Override
    public void close() {
        try {
            synchronized (internalSocket) {
                if (internalSocket.isClosed()) {
                    return;
                }
                internalSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void send(Message message) {
        try {
            synchronized (internalSocket) {
                if (internalSocket.isClosed()) {
                    return;
                }
                outputWriter.writeObject(message);
                outputWriter.flush();
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public boolean isClosed() {
        synchronized (internalSocket) {
            return internalSocket.isClosed();
        }
    }

}
