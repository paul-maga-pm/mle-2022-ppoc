package com.tora.chatapp.plugins.networking.serversocket;

public class JdkClientSocketConnectionProperties {

    private String ipAddress;
    private int port;

    public JdkClientSocketConnectionProperties() {
    }

    public JdkClientSocketConnectionProperties(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JdkClientSocketConnectionProperties)) return false;

        JdkClientSocketConnectionProperties that = (JdkClientSocketConnectionProperties) o;

        if (port != that.port) return false;
        return ipAddress != null ? ipAddress.equals(that.ipAddress) : that.ipAddress == null;
    }

    @Override
    public int hashCode() {
        int result = ipAddress != null ? ipAddress.hashCode() : 0;
        result = 31 * result + port;
        return result;
    }
}
