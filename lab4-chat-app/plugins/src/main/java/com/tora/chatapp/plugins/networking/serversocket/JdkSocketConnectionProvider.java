package com.tora.chatapp.plugins.networking.serversocket;

import com.tora.chatapp.core.plugins.networking.Connection;
import com.tora.chatapp.core.plugins.networking.ConnectionProvider;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class JdkSocketConnectionProvider implements ConnectionProvider {

    private final Set<JdkClientSocketConnectionProperties> nodes;

    public JdkSocketConnectionProvider(Set<JdkClientSocketConnectionProperties> nodes) {
        this.nodes = nodes;
    }

    @Override
    public Set<Connection> create() {

        Set<Connection> connections = new HashSet<>();

        for (var node : nodes) {
            try {
                Connection connection = new JdkSocketClientConnection(node);
                connections.add(connection);

            } catch (IOException exception) {
                System.out.println("Can't connect to " + node.getIpAddress() + " " + node.getPort());
            }
        }

        return connections;
    }
}
