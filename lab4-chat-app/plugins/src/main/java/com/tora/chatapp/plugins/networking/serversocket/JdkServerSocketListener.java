package com.tora.chatapp.plugins.networking.serversocket;

import com.tora.chatapp.core.plugins.networking.Connection;
import com.tora.chatapp.core.plugins.networking.ConnectionListener;
import com.tora.chatapp.core.service.events.UserEnteredChatRoomEvent;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class JdkServerSocketListener extends ConnectionListener {

    private final int port;
    private ServerSocket serverSocket;

    public JdkServerSocketListener(int port) throws IOException {
        this.port = port;

    }

    @Override
    public void start() {

        Thread thread = new Thread(() -> {
            try {
                serverSocket = new ServerSocket(port);
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (true) {
                try {
                    Socket client = serverSocket.accept();
                    Connection connection = new JdkSocketClientConnection(client);
                    this.notifyEventWasTriggered(new UserEnteredChatRoomEvent(connection));

                } catch (SocketException e) {
                    if (serverSocket.isClosed()) {
                        break;
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });
        thread.start();
    }

    @Override
    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException ignored) {

        }
    }
}
