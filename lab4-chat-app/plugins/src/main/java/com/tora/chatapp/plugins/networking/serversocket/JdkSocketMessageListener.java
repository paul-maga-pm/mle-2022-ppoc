package com.tora.chatapp.plugins.networking.serversocket;

import com.tora.chatapp.core.plugins.networking.Connection;
import com.tora.chatapp.core.plugins.networking.MessageListener;
import com.tora.chatapp.core.service.events.ConnectionWasClosedEvent;
import com.tora.chatapp.core.service.events.MessageWasSendEvent;
import com.tora.chatapp.core.service.requests.Message;
import com.tora.chatapp.core.service.requests.MessageType;

import java.io.IOException;

public class JdkSocketMessageListener extends MessageListener {


    public JdkSocketMessageListener() {

    }

    @Override
    public void start(Connection connection) {
        JdkSocketClientConnection jdkConnection = (JdkSocketClientConnection) connection;

        Thread thread = new Thread(() -> {
            var inputStream = jdkConnection.getInputReader();
            while (true) {
                try {
                            var message = (Message) inputStream.readObject();

                            if (message.getType() == MessageType.SEND) {
                                var event = new MessageWasSendEvent(message);
                                messageWasSendEventObservable.notifyEventWasTriggered(event);
                            } else if (message.getType() == MessageType.LOGOUT) {
                                var event = new ConnectionWasClosedEvent(jdkConnection);
                                connectionWasClosedEventObservable.notifyEventWasTriggered(event);
                                return;
                            }

                } catch (IOException | ClassNotFoundException e) {
                    if (jdkConnection.isClosed()) {
                        return;
                    }
                }
            }
        });

        thread.start();
    }


}
